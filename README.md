# Python Package Publisher Container Image Builder

## Overview

The intent of this repository is to extend the Alpine-based Python container
images by setting some useful environment variables, installing some OS
packages, building some useful Python packages, and then removing the extra
OS packages.

Specifically, it seeks to:

- Enable the build of most Python packages which do not have specific OS deps
- Enable the styling or linting of most Python packages.
- Enable the running of tests for most Python packages.
- Enable the publication of build Python packages to a PyPA repository.

This Docker image will definitely not work for building and testing _every_
possible Python library.  `psycopg2`, for instance, requires postgres
libraries, and `requests` would require `cryptography` which got installed
by `twine` but the OS components used to build it (e.g. Rust) get uninstalled
in the container.  In the event you are trying to build other than
"pure python" libraries, it is recommended you use this repo and
[Python Library](https://gitlab.com/tragio/builders/python/python-library) as
references.

## Using the container image

You can build it locally by running:

```docker build .```

or pull the most recent version from Docker Hub via:

```docker pull tragio/python:3.9```

Once you've pull it down, you can run it as python:

```docker run --rm -it tragio/python:3.9 <filename>.py```

or run a shell with code bind-mounted:

```docker run --rm -it -v "$(pwd)":/tmp/work tragio/python:3.9 ash```

or run a command directly with code bind-mounted, e.g.:

```
docker run --rm -it -v "$(pwd)":/tmp/work tragio/python:3.9 python <filename>.py
docker run --rm -it -v "$(pwd)":/tmp/work tragio/python:3.9 pipenv shell
```
