# provide injection point to override the base image python version
ARG CONTAINER_PYTHON_VERSION=3.9

# ~ 50 MB
FROM python:${CONTAINER_PYTHON_VERSION}-alpine

# direct pipenv to make venv in project dir
ENV PIPENV_VENV_IN_PROJECT 1

# needed by pipenv to activate shell
ENV LANG en_US.UTF-8
ENV LC_ALL C.UTF-8
ENV SHELL /bin/ash

# prevent cargo and pip and friends from imploding as non-root user without home
ARG CONTAINER_HOME
ENV HOME=${CONTAINER_HOME:-/tmp}

# set a reasonable default WORKDIR (users can override this and HOME on their own)
WORKDIR $HOME/work

RUN set -eux \
    && apk add --no-cache \
        # for setuptools_scm
        git \
    && apk add --no-cache --virtual .build-deps \
        # for crypto (for twine, paramiko, etc.) (~ 800 MB)
        cargo gcc libffi-dev musl-dev openssl-dev \
    && pip install --no-cache-dir --upgrade \
        # keep current
        pip \
        # uniform app/library build env
        pipenv \
        # alternative to pipenv
        pip-tools \
        # inherit version from scm
        setuptools_scm \
        # defacto style enforcement (pre-installed due to dep on gcc/regex)
        black \
        # pytest NOT here due to issues using host site packages in venv
        # publish packages (pre-installed due to dep on crypto)
        twine \
    # gives back ~400 MB for final image size of < 400 MB
    && apk del .build-deps \
    # gives back ~250 MB of cached data
    && rm -fr $HOME/.[a-z]* \
    # ensure home and working directory are globally writable for non-root user
    && chmod -R a+rw $HOME
